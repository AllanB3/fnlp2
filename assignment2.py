import nltk

#import brown corpus
from nltk.corpus import brown

# module for training a Hidden Markov Model and tagging sequences
from nltk.tag.hmm import HiddenMarkovModelTagger

# module for computing a Conditional Frequency Distribution
from nltk.probability import ConditionalFreqDist

# module for computing a Conditional Probability Distribution
from nltk.probability import ConditionalProbDist



import operator
import random
from math import log

class HMM:
  def __init__(self,train_data,test_data):
    self.train_data = train_data
    self.test_data = test_data
    self.states = []
    self.viterbi = []
    self.backpointer = []

  #compute sensor model using ConditionalProbDist with the estimator: Lidstone probability distribution with +0.01 added to the sample count f$
  def sensor_model(self,train_data):
    #don't forget to lowercase the observation otherwise it mismatches the test data
    data = []
    for t in train_data:
      [data.append((t, w.lower())) for (w,t) in t]

    sensor_FD = ConditionalFreqDist(data)
    self.sensor_PD = ConditionalProbDist(sensor_FD, nltk.probability.LidstoneProbDist, 0.01)
    self.states = [u'ADV', u'NOUN', u'ADP', u'PRON', u'DET', u'.', u'PRT', u'VERB', u'X', u'NUM', u'CONJ', u'ADJ']
    print "states: ",self.states,"\n\n"

    return self.sensor_PD, self.states


  #test point 1a
  def test_sensor(self):
    print "test sensor"
    t1 = log(self.sensor_PD['NOUN'].prob('fulton')) #-7.47600100017
    t2 = log(self.sensor_PD['X'].prob('fulton')) #-8.73745258755
    return t1,t2

  #compute transition model using ConditionalProbDist with the estimator: Lidstone probability distribution with +0.01 added to the sample cou$
  def transition_model(self,train_data):
    data = []
    for t in train_data:
      data.append(('<s>', t[0][1]))
      data.append((t[len(t) - 1][1], '</s>'))
      [data.append((t[i][1], t[i + 1][1])) for i in range(len(t) - 1)]

    # the data object should be an array of tuples of conditions and observations
    # in our case the tuples will be of the form (tag_(i),tag_(i+1))
    # DON'T FORGET TO ADD THE START SYMBOL <s> and the END SYMBOL </s>
    transition_FD = ConditionalFreqDist(data)
    self.transition_PD = ConditionalProbDist(transition_FD, nltk.probability.LidstoneProbDist, 0.01)


    return self.transition_PD

  
  #test point 1b
  def test_transition(self):
    print "test transition"
    transition_PD = self.transition_model(self.train_data)
    start = log(transition_PD['<s>'].prob('NOUN')) #-1.23663567316
    end = log(transition_PD['NOUN'].prob('</s>')) #-5.06941327085
    return start,end

  #train the HMM model
  def train(self):
    self.sensor_model(self.train_data)
    self.transition_model(self.train_data)
  
  def set_models(self,sensor_PD,transition_PD):
    self.sensor_PD = sensor_PD
    self.transition_PD = transition_PD
  
  #initialize data structures for tagging a new sentence
  #describe the data structures with comments
  #use the models stored in the variables: self.sensor_PD and self.transition_PD
  #input: first word in the sentence to tag
  def initiatlize(self,observation):
    del self.viterbi[:]
    del self.backpointer[:]
    #initialize for transition from <s> , begining of sentence
    #use log-probabilities

    #The Viterbi data structure here is a list of dictionaries with the ith dictionary pertaining to the ith element of the
    #sentence being tagged. Each dictionary has each possible state as its keys along with the probability of the observation
    #occuring in that state.

    first_viterbi = {}
    for s in self.states:
      first_viterbi[s] = ((0 - log(self.transition_PD['<s>'].prob(s), 2)) + (0 - log(self.sensor_PD[s].prob(observation), 2)))

    #initialize backpointer

    #Again, the backpointer is a list of dictionaries with each dictionary containing all the possible states as keys with each
    #associated with the most likely previous state.

    first_backpointer = {}
    for s in self.states:
      first_backpointer[s] = '<s>'

    self.viterbi.append(first_viterbi)
    self.backpointer.append(first_backpointer)

  #tag a new sentence using the trained model and already initialized data structures
  #use the models stored in the variables: self.sensor_PD and self.transition_PD
  #update the self.viterbi and self.backpointer datastructures
  #describe your implementation with comments
  #input: list of words
  def tag(self,observations):
    tags = []
    index = 0
    current_decision = []
    
    for t in range(1, len(observations)):
      next_viterbi = {}
      next_backpointer = {}
      for state in self.states:
        next_viterbi[state] = min([self.viterbi[index][s] + ((0 - log(self.transition_PD[s].prob(state), 2)) + (0 - log(self.sensor_PD[state].prob(observations[t])))) for s in self.states])
        next_backpointer[state] = min(self.viterbi[index], key=lambda s : self.viterbi[index][s] + (0 - log(self.transition_PD[s].prob(state))))

      self.viterbi.append(next_viterbi)
      self.backpointer.append(next_backpointer)
      index += 1
        
    last_viterbi = {}
    for state in self.states:
      if state == '</s>':
        last_viterbi[state] = min([self.viterbi[index][s] + (0 - log(self.transition_PD[s].prob(state))) for s in self.states])
      else:
        last_viterbi[state] = 0

    last_backpointer = {}
    for state in self.states:
        last_backpointer[state] = min(self.viterbi[index], key=lambda s : self.viterbi[index][s] + (0 - log(self.transition_PD[s].prob('</s>'))))

    self.viterbi.append(last_viterbi)
    self.backpointer.append(last_backpointer)
    #reconstruct the tag sequence using the backpointer
    #return the tag sequence corresponding to the best path as a list (order should match that of the words in the sentence)
    tags = []
    for i in range(1, len(self.backpointer)):
      for s in self.states:
        if s == min(self.viterbi[i], key=self.viterbi[i].get):
          tags.append(self.backpointer[i][s])

    return tags

#Question 4: train the HiddenMarkovModelTagger on training data with different tagset and evaluate
def compare_taggers(train_data_Brown, train_data_Universal,test_data_Brown,test_data_Universal):
  # train taggers using HiddenMarkovModelTagger
  tagger_Brown = HiddenMarkovModelTagger.train(train_data_Brown)
  tagger_Universal = HiddenMarkovModelTagger.train(train_data_Universal)
      
  # evaluate the hmm taggers on the test data
  # make sure to use the test data with the same label set as the train data on which the tagger was trained
  eval_Brown = tagger_Brown.evaluate(test_data_Brown)
  eval_Universal = tagger_Universal.evaluate(test_data_Universal)
  
  # comment on the results.
  answer1 = "The tagger using the universal tagset may be more accurate than the one using the Brown tagset because the universal tagset has more potential tags. This means that the model of each tag in the resultant HMM will be less general and will be less susceptible to underfitting."
  answer2 = "As the size of the test set decreases, so does the accuracy. This is because there is not as much training data to work with and as such, the probability distributions used to build the HMM less accurately reflect the nature of the corpus."

  return eval_Brown, eval_Universal, answer1, answer2



def main():
  #devide corpus in train and test data
  tagged_sentences_Brown = brown.tagged_sents(categories= 'news')

  test_size = 1000
  train_size = len(tagged_sentences_Brown)-1000

  train_data_Brown = tagged_sentences_Brown[:train_size]
  test_data_Brown = tagged_sentences_Brown[-test_size:]

  tagged_sentences_Universal = brown.tagged_sents(categories= 'news', tagset='universal')
  train_data_Universal = tagged_sentences_Universal[:train_size]
  test_data_Universal = tagged_sentences_Universal[-test_size:]


  #create instance of HMM class and initialize the training and test sets
  obj = HMM(train_data_Universal,test_data_Universal)
  
  #train HMM
  obj.train()
  
  #part A: test sensor model
  t1,t2 = obj.test_sensor()
  print t1,t2 #-7.47644570515 -8.54286093816
  if(abs(t1+7.47)<0.02 and abs(t2+8.54) < 0.02):
    print "PASSED test sensor\n"
  else:
    print "FAILED test sensor\n"

  #part A: test transition model
  start,end = obj.test_transition()
  print start,end #-1.23663567316 -5.06985925345
  if(abs(start+1.23)<0.02 and abs(end+5.06) < 0.02):
    print "PASSED test transition\n"
  else:
    print "FAILED test transition\n"


  #part B: test accuracy on test set
  result = []
  correct = 0
  incorrect = 0
  accuracy = 0
  for sentence in test_data_Universal:
    s = [word.lower() for (word,tag) in sentence]
    obj.initiatlize(s[0])
    tags = obj.tag(s)
    for i in range(0,len(sentence)):
      if sentence[i][1] == tags[i]:
        correct+=1
      else:
        incorrect+=1
  accuracy = 1.0*correct/(correct+incorrect)
  print "accuracy: ",accuracy #accuracy:  0.8659247022
  if(abs(accuracy-0.86)<0.01):
    print "PASSED test viterbi\n"
  else:
    print "FAILED test viterbi\n"

  #test part C
  eval_Brown, eval_Universal,answer1,answer2 = compare_taggers(train_data_Brown,train_data_Universal,test_data_Brown,test_data_Universal)
  print "compare: ",eval_Brown, eval_Universal #compare: 0.844926528128 0.886183810103
  if(abs(eval_Brown-0.84)<0.01 and abs(eval_Universal-0.88) < 0.01):
    print "PASSED test compare\n"
  else:
    print "FAILED test compare\n"

  print "Answer1: ",answer1, "\n"
  print "Answer2: ",answer2, "\n"

if __name__ == '__main__':
    
    main()
